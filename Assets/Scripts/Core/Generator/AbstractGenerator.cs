using Core.ObjectPooling;
using UnityEngine;

namespace Core.Generator {
	public abstract class AbstractGenerator<T> : MonoBehaviour where T : class, IPoolObject {
		[SerializeField] private PoolManager poolManager;
		[SerializeField] private string factoryName;
		public abstract void Generate();

		protected virtual void Start() {
		}

		protected virtual T Use() {
			return poolManager.Use<T>(factoryName);
		}
	}
}