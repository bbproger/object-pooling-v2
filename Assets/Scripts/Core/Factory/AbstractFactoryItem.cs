using UnityEngine;

namespace Core.Factory {
	public abstract class AbstractFactoryItem : MonoBehaviour {
		protected virtual void Start() {
		}

		protected virtual void FixedUpdate() {
		}

		protected virtual void Update() {
		}

		protected virtual void OnEnable() {
		}

		protected virtual void OnDisable() {
		}

		protected virtual void OnDestroy() {
		}
	}
}