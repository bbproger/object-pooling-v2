using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Factory {
	public class FactoryHolder : MonoBehaviour {
		[SerializeField] private List<AbstractFactory> factories;

		public AbstractFactoryItem Create(string factoryName) {
			var factory = GetFactory(factoryName);
			if (factory == null) {
				return null;
			}

			return factory.Create();
		}

		public AbstractFactory GetFactory(string factoryName) {
			var factory = factories.FirstOrDefault(abstractFactory => abstractFactory.FactoryName.Equals(factoryName));
			if (factory == null) {
				Debug.LogError("Can't find factory.");
				return null;
			}

			return factory;
		}
	}
}