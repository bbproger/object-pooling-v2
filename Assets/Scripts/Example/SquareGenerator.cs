using Core.Generator;
using UnityEngine;

namespace Example {
	public class SquareGenerator : AbstractGenerator<SquareItem> {
		[SerializeField] private int count;

		protected override void Start() {
			base.Start();
			Generate();
		}

		public override void Generate() {
			for (int i = 0; i < count; i++) {
				var item = Use();
				item.transform.position = Random.insideUnitCircle * 8f;
			}
		}
	}
}