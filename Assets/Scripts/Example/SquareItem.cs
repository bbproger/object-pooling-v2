using Core.Factory;
using UnityEngine;

namespace Example {
	public class SquareItem:AbstractFactoryPoolObjectItem {
		public override void Activate() {
			base.Activate();
			transform.localScale = Random.insideUnitSphere * 6f;
		}
	}
}