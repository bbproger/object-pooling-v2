﻿using Core.Generator;
using UnityEngine;

namespace Example {
	public class CircleGenerator : AbstractGenerator<CircleItem> {
		[SerializeField] private int minCount;
		[SerializeField] private int maxCount;

		protected override void Start() {
			base.Start();
			Generate();
		}

		public override void Generate() {
			var count = Random.Range(minCount, maxCount);
			for (int i = 0; i < count; i++) {
				var item = Use();
				item.transform.position = Random.insideUnitCircle * 5f;
			}
		}
	}
}