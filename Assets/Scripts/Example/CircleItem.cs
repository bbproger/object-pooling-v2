using Core.Factory;
using UnityEngine;

namespace Example {
	public class CircleItem : AbstractFactoryPoolObjectItem {
		[SerializeField] private SpriteRenderer spriteRenderer;

		public override void Activate() {
			base.Activate();
			spriteRenderer.color = new Color(Random.value, Random.value, Random.value, 1f);
		}

		public override void Deactivate() {
			base.Deactivate();
			spriteRenderer.color = Color.black;
		}
	}
}